#!/bin/sh

APP_NAME=api-rbac
BASE_PATH=$(dirname \"$0\" | tr -d '"')

if [ -z "$1" ]; then
  ENVIRONMENT="staging"
else
  ENVIRONMENT=$1
fi


echo "current cluster context:"
kubectl config current-context
kubectl get nodes
echo ""
echo "is this the right cluster? [yes|no]"
read CONFIRMATION
if [ ! "$CONFIRMATION" = "yes" ]; then
  exit 0
fi
echo "continuing..."


VALUES_BASE_YAML=$BASE_PATH/values.yaml
VALUES_ENV_YAML=$BASE_PATH/values.$ENVIRONMENT.yaml


helm template $APP_NAME \
              -f $VALUES_BASE_YAML \
              -f $VALUES_ENV_YAML \
              $BASE_PATH > $BASE_PATH/deploy-template.yaml

kubectl apply -f $BASE_PATH/deploy-template.yaml


echo "serviceaccount deployed!"
