# How to install/deploy the serviceaccount?
- switch to correct kube config context
- make sure that you did deploy k8s/rbac before trying to install the main stack
```shell
# export KUBECONFIG=$HOME/.kube/darkside-cluster
sh k8s/rbac/install.sh staging

sh k8s/rbac/install.sh prod

```
