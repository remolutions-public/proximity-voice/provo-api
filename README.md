# What is this?
This is the backend api which coordinates the provo electron apps



# Official Provo website
https://provo.remolutions.com/

[![alt text](https://provo.remolutions.com/assets/logos/Provo_64p.png "Provo")](https://provo.remolutions.com/)



## Provo Discord Server
- https://discord.gg/NJTFTgNvzG



## Donation/Support
If you like my work and want to support further development or just to spend me a coffee please

[![alt text](https://i.imgur.com/Y0XkUcd.png "Paypal $")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S3WQNNSVY8VAL)

[![alt text](https://i.imgur.com/xezX26q.png "Paypal €")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VQRPA46YADD9J)



# How to test hot config loading?
- modify .env.dev values:
  - NAMESPACE=provo-staging
- switch to correct kubernetes context locally before starting the api
```shell
# export KUBECONFIG=$HOME/.kube/darkside-cluster
npm start

```

