FROM bitnami/node:18 as builder

COPY . ./

ENV BUILD_DIR=/app/src/app/

COPY . ${BUILD_DIR}

WORKDIR ${BUILD_DIR}

RUN npm install
RUN npm run build


FROM bitnami/node:18

ARG ENV

# Set necessary environment variables.
ENV KUBECTL_VERSION="1.25.4"\
    NODE_ENV=production \
    NPM_CONFIG_PREFIX=/home/node/.npm-global \
    PATH=$PATH:/home/node/.npm-global/bin:/home/node/node_modules/.bin:$PATH \
    BUILD_DIR=/app/src/app/


# Create the working directory, including the node_modules folder for the sake of assigning ownership in the next command
RUN mkdir -p /app/src/app/node_modules

RUN apt-get update \
    && apt-get install libssl-dev gzip zlib1g zlib1g-dev curl -y \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
    && mv kubectl /usr/bin/kubectl \
    && chmod +x /usr/bin/kubectl


# Set the default working directory for the app
WORKDIR /app/src/app

# Copy package.json, package-lock.json
# Copying this separately prevents re-running npm install on every code change.
COPY --from=builder ${BUILD_DIR}/package*.json ./

# Install dependencies.
# RUN npm i -g @nestjs/cli
RUN npm ci --prod
# RUN npm install --prod

# Bundle app source
COPY --from=builder ${BUILD_DIR}/dist ./dist
RUN mkdir -p policies tokens audit


# Display directory structure
RUN ls -l

# Run the web service on container startup
CMD [ "npm", "run", "start:prod" ]

