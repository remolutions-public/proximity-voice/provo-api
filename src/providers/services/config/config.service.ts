import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { KubectlService } from '../kubectl/kubectl.service';
import {
  typeIsObject,
  typeIsJSON,
  parseBoolean,
} from '../../../libraries/parser.lib';
import { encodeBase64, typeIsBase64 } from '../../../libraries/base64.lib';
import { envVars } from '../../environment';

export class VoiceModeEntry {
  voiceLevel: number;
  maxDistancePercent: number;
}

export class VoiceModes {
  [mode: string]: VoiceModeEntry;
}

@Injectable()
export class GlobalConfigService {
  public cachedValidTenantIds: string[];

  private loggerContext = 'GlobalConfigService';
  private loggerInst: Logger;
  get logger(): Logger {
    if (!this.loggerInst)
      this.loggerInst = new Logger(GlobalConfigService.name);
    return this.loggerInst;
  }

  static get currentEnv(): string {
    return String(process.env.ENV || 'DEV')
      .toUpperCase()
      .trim();
  }

  clientRedisLifetimeSeconds = 10;
  clientPlayerRedisLifetimeSeconds = 5;

  defaultMaxVoiceConnectionDistance = 15000;
  maxVoiceConnectionDistance: number = this.defaultMaxVoiceConnectionDistance;

  defaultPositionWhisperOffsetMultiplier?: number = 1.0;
  positionWhisperOffsetMultiplier?: number =
    this.defaultPositionWhisperOffsetMultiplier;
  defaultRadiusWhisperExtensionMultiplier?: number = 1.5;
  radiusWhisperExtensionMultiplier?: number =
    this.defaultRadiusWhisperExtensionMultiplier;
  defaultPositionTalkOffsetMultiplier?: number = 1.0;
  positionTalkOffsetMultiplier?: number =
    this.defaultPositionTalkOffsetMultiplier;
  defaultRadiusTalkExtensionMultiplier?: number = 1.5;
  radiusTalkExtensionMultiplier?: number =
    this.defaultRadiusTalkExtensionMultiplier;
  defaultPositionYellOffsetMultiplier?: number = 1.0;
  positionYellOffsetMultiplier?: number =
    this.defaultPositionYellOffsetMultiplier;
  defaultRadiusYellExtensionMultiplier?: number = 1.0;
  radiusYellExtensionMultiplier?: number =
    this.defaultRadiusYellExtensionMultiplier;
  defaultPositionOffsetDirection?: number = 1;
  positionOffsetDirection?: number = this.defaultPositionOffsetDirection;

  // ORDERING FROM QUIET TO LOUDD IS IMPORTANT!
  // whisper    // default: P key         // ActionName="PushToWhisper"
  // normal     // default: B key         // ActionName="PushToTalk"
  // shouting   // default: DELETE key    // ActionName="PushToYell"
  // convert to one line string for env var with command: echo "HERE_GOES_THE_CONFIG" | tr -d ' ' | tr -d '\n'
  defaultVoiceModeKeys: VoiceModes = {
    whisper: {
      voiceLevel: 0.3,
      maxDistancePercent: 0.033,
    },
    talk: {
      voiceLevel: 0.8,
      maxDistancePercent: 0.11,
    },
    yell: {
      voiceLevel: 1.25,
      maxDistancePercent: 0.52,
    },
  };
  voiceModeKeys = this.defaultVoiceModeKeys;

  defaultAllowPlayerHiding = true;
  allowPlayerHiding: boolean = this.defaultAllowPlayerHiding;
  defaultMaxPlayerHidingSeconds = 300;
  maxPlayerHidingSeconds: number = this.defaultMaxPlayerHidingSeconds;
  defaultPlayerHidingCooldownSeconds = 60;
  playerHidingCooldownSeconds: number = this.defaultPlayerHidingCooldownSeconds;
  defaultHideAllRemotePlayers = false;
  hideAllRemotePlayers: boolean = this.defaultHideAllRemotePlayers;
  defaultMaskPlayerIdentification = true;
  maskPlayerIdentification: boolean = this.defaultMaskPlayerIdentification;
  defaultShowPlayerNames = false;
  showPlayerNames: boolean = this.defaultShowPlayerNames;

  cachedConfigmapData: any;
  lastConfigmapDataCacheTime: number;
  configmapDataCacheDelay = 30000;

  constructor(
    private readonly configService: ConfigService,
    private readonly kubectlService: KubectlService,
  ) {
    this.logger.log(`Loaded the config: --> .env`, this.loggerContext);
    this.ensureValues(Object.keys(envVars));
  }

  public getValue(key: string, throwOnMissing = true): string {
    const value = envVars[key] || this.configService.get(key);
    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }

    return value;
  }

  /**
   * API should fail hard, if requested variable is not available in .env.*
   * or ENV variables/secrets from CI pipeline (kubernetes ENV)
   */
  public ensureValues(keys: string[]): void {
    keys.forEach((key) => {
      if (!envVars[key] && !this.getValue(key, true)) {
        const msg =
          'Missing envirnoment variable, check your .env file and gitlab CI vars!';
        if (this.logger) this.logger.error(msg, undefined, this.loggerContext);
        // eslint-disable-next-line no-console
        else console.error('logger is undefined', this.loggerContext, msg);
        throw new Error(msg);
      }
    });
  }

  public getPort(): string {
    const key = 'API_PORT';
    return envVars[key] || this.getValue(key, true);
  }

  public isProduction(): boolean {
    return envVars['ENV'] === 'PROD';
  }

  public isQA(): boolean {
    return envVars['ENV'] === 'QA';
  }

  public isStaging(): boolean {
    return envVars['ENV'] === 'STAGING';
  }

  public isNginx(): boolean {
    return this.isProduction() || this.isQA() || this.isStaging();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public getLogModes(): any {
    const key = 'LOG_MODES';
    const values = envVars[key] || this.getValue(key, true);
    const retObj = {};
    if (values)
      values.split(',').forEach((value: string | number) => {
        retObj[value] = true;
      });
    return retObj;
  }

  async getConfigObjectData(): Promise<any> {
    const rndDelay = this.configmapDataCacheDelay + Math.random() * 1000 * 5;
    if (
      this.cachedConfigmapData == undefined ||
      this.lastConfigmapDataCacheTime == undefined ||
      new Date().valueOf() - this.lastConfigmapDataCacheTime > rndDelay
    ) {
      const namespace = this.getValue('NAMESPACE');
      const configObjName = this.getValue('CONFIG_OBJECT_NAME');
      this.lastConfigmapDataCacheTime = new Date().valueOf();
      const dataStr = (await this.kubectlService
        .execKubectlCommand(
          `kubectl get configmap ${configObjName} -n ${namespace} --ignore-not-found -ojsonpath={.data}`,
        )
        .catch((err) => {
          this.logger.error(err, undefined, this.loggerContext);
        })) as string;
      if (!(dataStr == undefined) && dataStr.length) {
        try {
          const dataObj = JSON.parse(dataStr);
          this.cachedConfigmapData = dataObj;
        } catch (e) {
          this.logger.error(e, undefined, this.loggerContext);
        }
      }
      if (this.cachedConfigmapData == undefined) {
        this.cachedConfigmapData = {};
      }
    }
    return this.cachedConfigmapData;
  }

  async getMaxVoiceDistance() {
    const data = await this.getConfigObjectData();
    if (!(data == undefined) && Object.keys(data).length) {
      const maxVoiceDistance = data.MAX_VOICE_CONNECTION_DISTANCE;
      if (!(maxVoiceDistance == undefined) && maxVoiceDistance.length) {
        try {
          const sanitizedString = maxVoiceDistance.replace(/\n/g, '');
          this.maxVoiceConnectionDistance = parseFloat(sanitizedString);
        } catch (err) {
          this.logger.error(err, undefined, this.loggerContext);
        }
      }
    }
    return this.maxVoiceConnectionDistance;
  }

  async getVoiceModeKeys() {
    const data = await this.getConfigObjectData();
    if (!(data == undefined) && Object.keys(data).length) {
      const voiceModeKeys = data.VOICE_MODE_KEYS;
      if (!(voiceModeKeys == undefined) && voiceModeKeys.length) {
        try {
          if (typeIsBase64(voiceModeKeys)) {
            const decodingResult = encodeBase64(voiceModeKeys)?.replace(/\n/g, '');
            let obj: any;
            eval('obj=' + decodingResult);
            this.voiceModeKeys = obj;
          } else {
            const sanitizedString = voiceModeKeys.replace(/\n/g, '');
            let obj: any;
            eval('obj=' + sanitizedString);
            this.voiceModeKeys = obj;
          }
        } catch (err) {
          this.logger.error(err, undefined, this.loggerContext);
        }
      }
    }
    return this.voiceModeKeys;
  }

  async getMaskingConfig() {
    const data = await this.getConfigObjectData();
    if (!(data == undefined) && Object.keys(data).length) {
      const allowPlayerHiding = data.ALLOW_PLAYER_HIDING;
      if (!(allowPlayerHiding == undefined)) {
        this.allowPlayerHiding = parseBoolean(allowPlayerHiding);
      }
      const maxPlayerHidingSeconds = data.MAX_PLAYER_HIDING_SECONDS;
      if (!(maxPlayerHidingSeconds == undefined)) {
        this.maxPlayerHidingSeconds = parseInt(maxPlayerHidingSeconds, 10);
      }
      const playerHidingCooldownSeconds = data.PLAYER_HIDING_COOLDOWN_SECONDS;
      if (!(playerHidingCooldownSeconds == undefined)) {
        this.playerHidingCooldownSeconds = parseInt(
          playerHidingCooldownSeconds,
          10,
        );
      }
      const hideAllRemotePlayers = data.HIDE_ALL_REMOTE_PLAYERS;
      if (!(hideAllRemotePlayers == undefined)) {
        this.hideAllRemotePlayers = parseBoolean(hideAllRemotePlayers);
      }
      const maskPlayerIdentification = data.MASK_PLAYER_IDENTIFICATION;
      if (!(maskPlayerIdentification == undefined)) {
        this.maskPlayerIdentification = parseBoolean(maskPlayerIdentification);
      }
      const showPlayerNames = data.SHOW_PLAYER_NAMES;
      if (!(showPlayerNames == undefined)) {
        this.showPlayerNames = parseBoolean(showPlayerNames);
      }
    }
    return {
      allowPlayerHiding: this.allowPlayerHiding,
      maxPlayerHidingSeconds: this.maxPlayerHidingSeconds,
      playerHidingCooldownSeconds: this.playerHidingCooldownSeconds,
      hideAllRemotePlayers: this.hideAllRemotePlayers,
      maskPlayerIdentification: this.maskPlayerIdentification,
      showPlayerNames: this.showPlayerNames,
    };
  }

  async getPositionOffsetMultipliers() {
    // TODO
    const data = await this.getConfigObjectData();
    if (!(data == undefined) && Object.keys(data).length) {
      // whisper
      const positionWhisperOffsetMultiplier =
        data.POSTITION_WHISPER_OFFSET_MULTIPLIER;
      if (!(positionWhisperOffsetMultiplier == undefined)) {
        this.positionWhisperOffsetMultiplier = parseFloat(
          positionWhisperOffsetMultiplier,
        );
      }
      const radiusWhisperExtensionMultiplier =
        data.RADIUS_WHISPER_EXTENSION_MULTIPLIER;
      if (!(radiusWhisperExtensionMultiplier == undefined)) {
        this.radiusWhisperExtensionMultiplier = parseFloat(
          radiusWhisperExtensionMultiplier,
        );
      }
      // talk
      const positionTalkOffsetMultiplier =
        data.POSTITION_TALK_OFFSET_MULTIPLIER;
      if (!(positionTalkOffsetMultiplier == undefined)) {
        this.positionTalkOffsetMultiplier = parseFloat(
          positionTalkOffsetMultiplier,
        );
      }
      const radiusTalkExtensionMultiplier =
        data.RADIUS_TALK_EXTENSION_MULTIPLIER;
      if (!(radiusTalkExtensionMultiplier == undefined)) {
        this.radiusTalkExtensionMultiplier = parseFloat(
          radiusTalkExtensionMultiplier,
        );
      }
      // yell
      const positionYellOffsetMultiplier =
        data.POSTITION_YELL_OFFSET_MULTIPLIER;
      if (!(positionYellOffsetMultiplier == undefined)) {
        this.positionYellOffsetMultiplier = parseFloat(
          positionYellOffsetMultiplier,
        );
      }
      const radiusYellExtensionMultiplier =
        data.RADIUS_YELL_EXTENSION_MULTIPLIER;
      if (!(radiusYellExtensionMultiplier == undefined)) {
        this.radiusYellExtensionMultiplier = parseFloat(
          radiusYellExtensionMultiplier,
        );
      }
      // all
      const positionOffsetDirection = data.POSITION_OFFSET_DIRECTION;
      if (!(positionOffsetDirection == undefined)) {
        this.positionOffsetDirection = parseFloat(positionOffsetDirection);
      }
    }
    return {
      positionWhisperOffsetMultiplier: this.positionWhisperOffsetMultiplier,
      radiusWhisperExtensionMultiplier: this.radiusWhisperExtensionMultiplier,
      positionTalkOffsetMultiplier: this.positionTalkOffsetMultiplier,
      radiusTalkExtensionMultiplier: this.radiusTalkExtensionMultiplier,
      positionYellOffsetMultiplier: this.positionYellOffsetMultiplier,
      radiusYellExtensionMultiplier: this.radiusYellExtensionMultiplier,
      positionOffsetDirection: this.positionOffsetDirection,
    };
  }

  getMqttPassword(username: string): { mqttUser: string; mqttPass: string } {
    const users = this.getValue('RABBITMQ_USERS')?.split(',');
    let mqttUser: string;
    let mqttPass: string;
    for (const user of users) {
      const userSplit = user.split(':');
      if (userSplit[0] === username) {
        mqttUser = userSplit[0];
        mqttPass = userSplit[1];
        break;
      }
    }
    return { mqttUser, mqttPass };
  }
}
