import { Injectable } from '@nestjs/common';
import { GlobalConfigService } from '../config/config.service';
import { GlobalRedisService, EConnection } from '../redis/redis.service';
import { Logger } from '@nestjs/common';
import { distance3DTo } from '../../../libraries/distance.lib';
import * as mqtt from 'mqtt';
import {
  debounce,
  stopDebounceRef,
  hasDebounceRef,
} from '../../../libraries/debounce.lib';
import * as protobufjs from 'protobufjs';
import { voiceBaseSettingsSchema } from '../../../protobufModels/voice-base-settings';
import { nearbyPlayersSchema } from '../../../protobufModels/nearby-players';
import { playerDataSchema } from '../../../protobufModels/player-data';

@Injectable()
export class GlobalMqttService {
  private loggerContext = 'GlobalMqttService';
  private brokerMqttConnection: mqtt.MqttClient;

  private heartbeatIntervalMs = 1000;

  brokerLatency = 999;
  lastBrokerLatencyTime: number;
  latencyTimeout = 2500;
  healthState = 'disconnected';

  podName = this.configService.getValue('POD_NAME');
  podIp = this.configService.getValue('POD_IP');

  controllerChannelName =
    'controller-' + this.configService.getValue('POD_NAME');

  protobufRoot: protobufjs.Root;

  constructor(
    private readonly configService: GlobalConfigService,
    private readonly logger: Logger,
    private readonly redisService: GlobalRedisService,
  ) {
    if (
      (this.configService.getValue('MODE') || '').trim().toLowerCase() !==
      'agent'
    ) {
      this.loadProtobufSchemas();
      this.connectController();
    }
  }

  loadProtobufSchemas() {
    this.protobufRoot = protobufjs.parse(voiceBaseSettingsSchema).root;
    this.protobufRoot.add(protobufjs.parse(nearbyPlayersSchema).root);
    this.protobufRoot.add(protobufjs.parse(playerDataSchema).root);
  }

  retryConnect() {
    if (hasDebounceRef('retryConnect')) {
      return;
    }
    debounce('retryConnect', this.latencyTimeout, async () => {
      if (this.healthState !== 'healthy') {
        this.logger.warn('reconnecting to mqtt broker...');
        await this.connectController();
      }
      if (this.healthState !== 'healthy') {
        this.retryConnect();
      }
    });
  }

  async connectController() {
    const brokerTarget = this.configService.getValue('RABBITMQ_SERVICE_NAME');
    const mqttPort = this.configService.getValue('RABBITMQ_MQTT_PORT');

    const clientUsername = 'provo_api';
    const mqttCreds = this.configService.getMqttPassword(clientUsername);

    if (brokerTarget == undefined) {
      this.logger.error(
        'brokerTarget not set, aborting amqp connection attempt...',
        undefined,
        this.loggerContext,
      );
      return;
    }

    if (this.brokerMqttConnection?.connected) {
      this.brokerMqttConnection.end();
    }

    this.healthState = 'connecting';
    const brokerUrl = `mqtt://${mqttCreds.mqttUser}:${mqttCreds.mqttPass}@${brokerTarget}:${mqttPort}`;
    this.brokerMqttConnection = mqtt.connect(brokerUrl);
    if (this.brokerMqttConnection == undefined) {
      return;
    }

    this.brokerMqttConnection.on('connect', () => {
      this.logger.debug('mqtt controller client connected', this.loggerContext);
      this.healthState = 'connected';
      stopDebounceRef('TimeoutConnectController');
      this.brokerMqttConnection.on('message', (topic, message) => {
        if (topic === this.controllerChannelName) {
          this.controllerReceiver(message);
        }
      });

      this.brokerMqttConnection.subscribe(this.controllerChannelName, (err) => {
        if (!(err == undefined)) {
          this.logger.error(err, undefined, this.loggerContext);
          this.healthState = 'failed';
          stopDebounceRef('TimeoutConnectController');
          this.retryConnect();
        } else {
          this.sendControllerHeartbeat();
        }
      });
    });

    debounce('TimeoutConnectController', 5000, () => {
      this.logger.error(
        'mqtt connection timeout, retrying...',
        undefined,
        this.loggerContext,
      );
      this.healthState = 'timeout';
      this.retryConnect();
    });
  }

  async controllerReceiver(msg: Buffer) {
    try {
      if (!(msg == undefined)) {
        const data = JSON.parse(msg.toString());
        if (!(data.clientid == undefined)) {
          this.receiveClientData(data);
        } else {
          this.brokerLatency = new Date().valueOf() - data.time || 0;
          this.lastBrokerLatencyTime = new Date().valueOf();
          this.healthState = 'healthy';
        }
      } else {
        this.logger.warn('Consumer cancelled by server, reopening...');
        this.healthState = 'failing';
        this.brokerMqttConnection?.end();
        this.retryConnect();
      }
    } catch (err) {
      this.logger.error(err, undefined, this.loggerContext);
    }
  }

  sendControllerHeartbeat() {
    if (this.brokerMqttConnection == undefined) {
      this.logger.error(
        'heartbeat failed, mqtt client is undefined',
        undefined,
        this.loggerContext,
      );
      return;
    }

    this.brokerMqttConnection
      .publishAsync(
        this.controllerChannelName,
        Buffer.from(
          JSON.stringify({
            time: new Date().valueOf(),
          }),
        ),
      )
      .catch((err) => {
        this.logger.error(err, undefined, this.loggerContext);
      });

    if (
      new Date().valueOf() - this.lastBrokerLatencyTime >
      this.latencyTimeout
    ) {
      this.healthState = 'timeout';
      this.retryConnect();
    } else {
      debounce('sendControllerHeartbeat', this.heartbeatIntervalMs, () => {
        this.sendControllerHeartbeat();
      });
    }
  }

  //////////////////////////////////////////
  ///////////////// webRTC /////////////////
  //////////////////////////////////////////
  async receiveClientData(data: any) {
    const remoteClientChannelName = this.sanitizeClientChannelName(
      data.clientid,
    );
    let messageType: protobufjs.Type;
    let message: protobufjs.Message<{}>;
    let b64content: Uint8Array;
    let decodedData: any;
    switch (data.type) {
      case 'heartbeat':
        this.brokerMqttConnection.publish(
          remoteClientChannelName,
          Buffer.from(JSON.stringify(data)),
        );
        break;
      case 'PlayerData':
      case 'debug':
        messageType = this.protobufRoot.lookupType('PlayerData');
        b64content = new Uint8Array(Buffer.from(data.content, 'base64'));
        message = messageType.decode(b64content);
        decodedData = messageType.toObject(message);
        decodedData.apiName = this.podName;
        decodedData.apiIp = this.podIp;
        decodedData.clientid = data.clientid;
        // we do not need to wait here, since the result of sendNearbyPlayers should not even include this same player.
        this.redisService
          .set(
            EConnection.default,
            'realms:' + decodedData.realm + ':' + decodedData.clientid,
            decodedData,
            this.configService.clientPlayerRedisLifetimeSeconds,
          )
          .catch((err) =>
            this.logger.error(
              err,
              undefined,
              this.loggerContext + '-receiveClientData-playerData',
            ),
          );
        this.sendNearbyPlayers(decodedData);
        break;
      case 'fetch-coturn-data':
        const coturnData = await this.redisService
          .getCoturnData()
          .catch((err) => {
            this.logger.error(
              err.message,
              undefined,
              this.loggerContext + '-receiveClientData-fetch-coturn-data',
            );
          });

        if (!(coturnData == undefined)) {
          this.brokerMqttConnection.publish(
            remoteClientChannelName,
            Buffer.from(
              JSON.stringify({
                type: 'receive-coturn-data',
                request: data,
                coturnData: coturnData,
              }),
            ),
          );
        }
        break;
      case 'fetch-voiceBaseSettings':
        const voiceBaseSettings = {
          maxDistance: await this.configService.getMaxVoiceDistance(),
          voiceModeKeys: await this.configService.getVoiceModeKeys(),
          velocityOffsets:
            await this.configService.getPositionOffsetMultipliers(),
        };
        try {
          const messageType = this.protobufRoot.lookupType('VoiceBaseSettings');
          const protobufMsg = messageType.encode(voiceBaseSettings).finish();
          this.brokerMqttConnection.publish(
            remoteClientChannelName,
            JSON.stringify({
              type: 'VoiceBaseSettings',
              content: Buffer.from(protobufMsg).toString('base64'),
            }),
          );
        } catch (e) {
          this.logger.error(e, undefined, this.loggerContext);
        }
        break;
      case 'fetch-maskingConfig':
        this.brokerMqttConnection.publish(
          remoteClientChannelName,
          Buffer.from(
            JSON.stringify({
              type: 'receive-maskingConfig',
              request: data,
              maskConfig: await this.configService.getMaskingConfig(),
            }),
          ),
        );
        break;
      case 'init-rtc-handshake':
      case 'resolve-rtc-handshake':
      case 'send-ice-candidate':
      case 'send-rtc-offer':
      case 'send-rtc-answer':
      case 'send-rtc-closing':
      case 'audiocheck':
      case 'reconnect':
        this.brokerMqttConnection.publish(
          remoteClientChannelName,
          Buffer.from(JSON.stringify(data)),
        );
        break;
      default:
        console.warn('uncatched receiveClientData:', data);
    }
  }

  private cachedRealmPlayers: {
    [realm: string]: {
      timestamp: number;
      playerObjs: any[];
    };
  } = {};
  private maxRealmPlayersCacheTime = 90;
  private cachedRealmPlayerKeys: {
    [realm: string]: {
      timestamp: number;
      keys: string[];
    };
  } = {};
  private maxRealmPlayerKeysCacheTime = 750;

  async sendNearbyPlayers(data: any) {
    const nearbyPlayers = [];

    if (
      this.cachedRealmPlayers[data.realm]?.playerObjs == undefined ||
      new Date().valueOf() - this.cachedRealmPlayers[data.realm]?.timestamp >
        this.maxRealmPlayersCacheTime
    ) {
      // we try to reduce stress on redis and waiting time of this api by caching player keys for a longer period
      let allKeys = this.cachedRealmPlayerKeys[data.realm]?.keys;
      if (
        allKeys == undefined ||
        new Date().valueOf() -
          this.cachedRealmPlayerKeys[data.realm]?.timestamp >
          this.maxRealmPlayerKeysCacheTime
      ) {
        allKeys = await this.redisService.scanKeys(
          EConnection.default,
          'realms:' + data.realm + ':*',
        );
        if (this.cachedRealmPlayerKeys[data.realm] == undefined) {
          this.cachedRealmPlayerKeys[data.realm] = <any>{};
        }
        this.cachedRealmPlayerKeys[data.realm].keys = allKeys;
        this.cachedRealmPlayerKeys[data.realm].timestamp = new Date().valueOf();
      }

      // caching player data as much in parallel as possible
      const playerObjs = [];
      const playerDataLoadingPromises: Promise<any>[] = [];
      for (const key of allKeys) {
        playerDataLoadingPromises.push(
          this.redisService
            .getValue(EConnection.default, key)
            .then((result) => {
              return { otherPlayer: result, key };
            }),
        );
      }

      await Promise.all(playerDataLoadingPromises).then(async (results) => {
        for (const result of results) {
          if (result.otherPlayer == undefined) {
            continue;
          }
          playerObjs.push(result.otherPlayer);
          const keyId = result.key.split(':')[2];
          if (keyId !== data.clientid) {
            if (
              !(result.otherPlayer?.id == undefined) &&
              String(result.otherPlayer.id) !== '0'
            ) {
              if (
                distance3DTo(data.pos, result.otherPlayer.pos) <=
                parseFloat(
                  (await this.configService.getMaxVoiceDistance()) as any,
                )
              ) {
                nearbyPlayers.push(result.otherPlayer);
              }
            }
          }
        }
      });

      this.cachedRealmPlayers[data.realm] = {
        timestamp: new Date().valueOf(),
        playerObjs,
      };
    } else {
      const playerObjs = this.cachedRealmPlayers[data.realm]?.playerObjs;
      for (const otherPlayer of playerObjs) {
        if (
          !(otherPlayer?.id == undefined) &&
          String(otherPlayer.id) !== '0' &&
          otherPlayer.clientid !== data.clientid
        ) {
          if (
            distance3DTo(data.pos, otherPlayer.pos) <=
            parseFloat((await this.configService.getMaxVoiceDistance()) as any)
          ) {
            nearbyPlayers.push(otherPlayer);
          }
        }
      }
    }

    const messageType = this.protobufRoot.lookupType('NearbyPlayers');
    const protobufMsg = messageType.encode({ players: nearbyPlayers }).finish();
    this.brokerMqttConnection.publish(
      this.sanitizeClientChannelName(data.clientid),
      JSON.stringify({
        type: 'NearbyPlayers',
        content: Buffer.from(protobufMsg).toString('base64'),
      }),
    );
  }

  sanitizeClientChannelName(clientid: string) {
    if (clientid == undefined) {
      return;
    }
    return 'client-' + clientid.replace(/client-/g, '');
  }
}
