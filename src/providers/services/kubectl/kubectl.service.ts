import { Injectable, Logger } from '@nestjs/common';
import { exec } from 'child_process';

@Injectable()
export class KubectlService {
  private loggerContext = 'KubectlService';

  constructor(
    private readonly logger: Logger,
  ) {}

  async execKubectlCommand(command: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      exec(command, async (error, stdout, stderr) => {
        if (error) {
          this.logger.error(
            `error: ${error.message}`,
            undefined,
            this.loggerContext,
          );
          const errMessage = error.message || error;
          return reject(errMessage);
        }
        if (
          !(stderr == undefined) &&
          stderr.length &&
          String(stderr).indexOf('Unable to use a TTY') === -1
        ) {
          this.logger.error(`stderr: ${stderr}`, undefined, this.loggerContext);
          return reject({ message: stderr });
        }
        if (stdout == undefined || !stdout.length) {
          return resolve('');
        }
        resolve(stdout);
      });
    });
  }
}
