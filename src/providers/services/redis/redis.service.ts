import { Injectable } from '@nestjs/common';
import { Redis } from 'ioredis';
import { InjectRedis } from '@liaoliaots/nestjs-redis';
import { GlobalConfigService } from '../config/config.service';
import { Logger } from '@nestjs/common';
import { envVars } from '../../../providers/environment';
import { typeIsObject, typeIsJSON } from '../../../libraries/parser.lib';
import { v4 as uuidv4 } from 'uuid';

export enum EConnection {
  'default',
  'coturn',
}

@Injectable()
export class GlobalRedisService {
  private loggerContext = 'GlobalRedisService';

  constructor(
    @InjectRedis(envVars.REDIS_NAME)
    private readonly redisDefaultService: Redis,
    private readonly globalConfigService: GlobalConfigService,
    private readonly logger: Logger,
  ) {}

  getRedisConnection(redisName?: EConnection) {
    return this.redisDefaultService;
  }

  async getRedisHealth(): Promise<boolean> {
    if (this.redisDefaultService.status === 'ready') {
      return true;
    }
    return false;
  }

  async update(
    connection: EConnection,
    key: string,
    partialBody: any | JSON,
    overrideTtlInSeconds?: number,
  ): Promise<'OK'> {
    return new Promise(async (resolve, reject) => {
      partialBody.updatedAt = new Date();
      const foundObj = (await this.getValue(
        connection,
        key,
        undefined,
        true,
      ).catch((err) => {
        reject(err);
      })) as any;
      const mergeObj = (obj: any, updateObj: any) => {
        for (const attr of Object.keys(obj)) {
          if (obj[attr] === null) {
            delete updateObj[attr];
          } else if (typeIsObject(obj[attr]) && typeIsObject(updateObj[attr])) {
            mergeObj(obj[attr], updateObj[attr]);
          } else {
            updateObj[attr] = obj[attr];
          }
        }
      };
      mergeObj(partialBody, foundObj);
      const updateResult = (await this.set(
        connection,
        key,
        foundObj,
        overrideTtlInSeconds,
        overrideTtlInSeconds == undefined ? true : undefined,
      ).catch((err) => {
        reject(err);
      })) as 'OK';
      if (updateResult == undefined) {
        return reject(new Error('update failed'));
      }
      resolve(updateResult);
    });
  }

  async set(
    connection: EConnection,
    key: string,
    value: any | JSON,
    ttlInSeconds: number = 2592000,
    keepttl?: boolean,
  ): Promise<'OK'> {
    return new Promise(async (resolve, reject) => {
      if (typeIsObject(value)) {
        if (!(value.redisKey == undefined)) {
          delete value.redisKey;
        }
        value = JSON.stringify(value);
      }
      if (keepttl) {
        ttlInSeconds = (await this.getRedisConnection(connection)
          .ttl(key)
          .catch((err: any) => {
            reject(err);
          })) as number;
      }
      if (ttlInSeconds <= 0) {
        await this.getRedisConnection(connection)
          .set(key, value)
          .then((result: any) => {
            resolve(result);
          })
          .catch((err: any) => {
            reject(err);
          });
      } else {
        await this.getRedisConnection(connection)
          .set(key, value, 'EX', ttlInSeconds)
          .then((result: any) => {
            resolve(result);
          })
          .catch((err: any) => {
            reject(err);
          });
      }
    });
  }

  async hasKey(connection: EConnection, key: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      await this.getRedisConnection(connection)
        .exists(key)
        .then((result: any) => {
          if (result > 0) {
            return resolve(true);
          }
          resolve(false);
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  }

  async getAllKeys(
    connection: EConnection,
    keyPrefix: string,
  ): Promise<string[]> {
    return new Promise(async (resolve, reject) => {
      await this.getRedisConnection(connection)
        .keys(keyPrefix + '*')
        .then((result: any) => {
          resolve(result);
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  }

  async scanKeys(
    connection: EConnection,
    keyPrefix: string,
    cursor?: number,
    count = 25,
    keys = [],
    noRecursion?: boolean,
  ): Promise<string[]> {
    const result = await this.getRedisConnection(connection)
      .scan(cursor, 'MATCH', keyPrefix, 'COUNT', Math.max(count, 3))
      .catch((err: any) => {
        this.logger.error(err);
      });

    if (result == undefined) {
      return [];
    }

    cursor = result[0];
    const batch = result[1];

    keys.push(...batch);

    if (!noRecursion && cursor > 0) {
      return this.scanKeys(connection, keyPrefix, cursor, count, keys);
    }

    return keys;
  }

  async getValue(
    connection: EConnection,
    key: string,
    select?: string[],
    selectAll?: boolean,
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const value = (await this.getRedisConnection(connection)
        .get(key)
        .catch((err: any) => reject(err))) as string;
      if (typeIsJSON(value)) {
        const data = this.filterHiddenAttributes(
          JSON.parse(value),
          select,
          selectAll,
        );
        data.redisKey = key;
        return resolve(data);
      }
      return resolve(value);
    });
  }

  async getValues(
    connection: EConnection,
    keys: string[],
    select?: string[],
    selectAll?: boolean,
  ): Promise<any[]> {
    return new Promise(async (resolve, reject) => {
      const promises = [];
      const values = [];
      for (const key of keys) {
        const value = (await this.getRedisConnection(connection)
          .get(key)
          .catch((err: any) => {
            this.logger.error(err, undefined, this.loggerContext);
          })) as string;
        if (value == undefined) {
          return;
        }
        const data = this.filterHiddenAttributes(value, select, selectAll);
        data.redisKey = key;
        values.push(data);
      }
      return resolve(values);
    });
  }

  async remove(connection: EConnection, key: string): Promise<number> {
    return new Promise(async (resolve, reject) => {
      if (key == undefined || !key.length) {
        return resolve(0);
      }
      try {
        const result = await this.getRedisConnection(connection).del(key);
        resolve(result || 0);
      } catch (e) {
        reject(e);
      }
    });
  }

  async delete(connection: EConnection, key: string): Promise<number> {
    return this.remove(connection, key);
  }

  private filterHiddenAttributes(
    data: any,
    select?: string[],
    selectAll?: boolean,
  ) {
    if (selectAll) {
      return data;
    }
    for (const attr of Object.keys(data)) {
      if (typeIsObject(data[attr])) {
        if (data[attr].hidden) {
          if (!(select == undefined) && select.indexOf(attr) > -1) {
            continue;
          }
          delete data[attr];
        }
      }
    }
    return data;
  }

  //////////////////////////////////////////////////
  //////////////// HELPER FUNCTIONS ////////////////
  //////////////////////////////////////////////////
  private cachedCoturnData: any;
  private coturnDataCacheDelay = 5000;
  private lastCoturnDataCacheTime: number;
  async getCoturnData(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (
        !(this.lastCoturnDataCacheTime == undefined) &&
        new Date().valueOf() - this.lastCoturnDataCacheTime <=
          this.coturnDataCacheDelay &&
        !(this.cachedCoturnData == undefined)
      ) {
        return resolve(this.cachedCoturnData);
      }
      const keys: any =
        (await this.scanKeys(EConnection.coturn, 'coturn:*').catch((err) =>
          reject(err),
        )) || [];
      const coturnServers = [];
      if (!(keys == undefined) && keys.length) {
        const coturnDataPromises = [];
        for (const key of keys) {
          coturnDataPromises.push(this.getValue(EConnection.coturn, key));
          await Promise.all(coturnDataPromises)
            .then((dataEntries) => {
              for (const data of dataEntries) {
                if (!(data == undefined)) {
                  const result = {};
                  for (const k of Object.keys(data)) {
                    if (k.toLocaleLowerCase().indexOf('admin') === -1) {
                      result[k] = data[k];
                    }
                  }
                  coturnServers.push(result);
                }
              }
            })
            .catch((err) => {
              this.logger.error(
                err.message,
                undefined,
                this.loggerContext + '-getCoturnData',
              );
            });
        }
      }
      this.cachedCoturnData = coturnServers;
      this.lastCoturnDataCacheTime = new Date().valueOf();
      resolve(this.cachedCoturnData);
    });
  }

  private cachedBackendId: string;
  async getBackendId(): Promise<string> {
    if (!(this.cachedBackendId == undefined)) {
      return this.cachedBackendId;
    }
    let errorState: any;
    let backenObj = await this.getValue(EConnection.default, 'backend').catch(
      (err) => {
        errorState = err;
      },
    );
    if (!(errorState == undefined)) {
      this.logger.error(errorState, undefined, this.loggerContext);
      return;
    }

    if (!(backenObj?.backendid == undefined)) {
      this.cachedBackendId = backenObj?.backendid;
      return this.cachedBackendId;
    }

    if (backenObj == undefined) {
      backenObj = {};
    }
    backenObj.backendid = uuidv4();
    await this.set(EConnection.default, 'backend', backenObj, -1).catch(
      (err) => {
        errorState = err;
      },
    );
    if (!(errorState == undefined)) {
      this.logger.error(errorState, undefined, this.loggerContext);
      return;
    }

    this.cachedBackendId = backenObj.backendid;
    return this.cachedBackendId;
  }
}
