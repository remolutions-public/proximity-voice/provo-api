import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { GlobalConfigService } from '../services/config/config.service';
import { parseBoolean } from '../../libraries/parser.lib';
import { Observable } from 'rxjs';

@Injectable()
export class SecretGuard implements CanActivate {
  private isPublic = parseBoolean(
    this.globalConfigService.getValue('IS_PUBLIC_API'),
  );

  constructor(private readonly globalConfigService: GlobalConfigService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    if (this.isPublic) {
      return true;
    }

    const request = context.switchToHttp().getRequest();

    if (
      !(request.headers == undefined) &&
      request.headers.secret ===
        this.globalConfigService.getValue('GUARD_SECRET')
    ) {
      return true;
    }

    return false;
  }
}
