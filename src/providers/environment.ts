import * as dotenv from 'dotenv';
import * as path from 'path';

export const sourceEnv = () => {
  if (process.env.ENV === 'DEV' || process.env.ENV == undefined) {
    dotenv.config({ path: path.join(__dirname, '../../.env.dev') });
  }

  return {
    ENV: process.env.ENV,
    MODE: process.env.MODE,
    BACKEND_URL: process.env.BACKEND_URL,
    PROVO_CONTROLLER_URLS: process.env.PROVO_CONTROLLER_URLS,
    IS_PUBLIC_API: process.env.IS_PUBLIC_API,
    BACKEND_NAME: process.env.BACKEND_NAME,
    POD_IP: process.env.POD_IP,
    POD_NAME: process.env.POD_NAME,
    APP_NAME: process.env.APP_NAME,
    NAMESPACE: process.env.NAMESPACE,
    CONFIG_OBJECT_NAME: process.env.CONFIG_OBJECT_NAME,
    LOG_MODES: process.env.LOG_MODES,
    API_PORT: process.env.API_PORT,
    RABBITMQ_EXTERNAL_HOST: process.env.RABBITMQ_EXTERNAL_HOST,
    RABBITMQ_SERVICE_NAME: process.env.RABBITMQ_SERVICE_NAME,
    RABBITMQ_AMQP_PORT: process.env.RABBITMQ_AMQP_PORT,
    RABBITMQ_MQTT_PORT: process.env.RABBITMQ_MQTT_PORT,
    RABBITMQ_MQTT_WS_PORT: process.env.RABBITMQ_MQTT_WS_PORT,
    RABBITMQ_MQTT_EXTERNAL_WS_PORT: process.env.RABBITMQ_MQTT_EXTERNAL_WS_PORT,
    RABBITMQ_USERS: process.env.RABBITMQ_USERS,
    REDIS_USE_SENTINEL: process.env.REDIS_USE_SENTINEL,
    REDIS_SENTINEL_HOST: process.env.REDIS_SENTINEL_HOST,
    REDIS_HOST: process.env.REDIS_HOST,
    REDIS_SENTINEL_PORT: process.env.REDIS_SENTINEL_PORT,
    REDIS_PORT: process.env.REDIS_PORT,
    REDIS_SENTINEL_NAME: process.env.REDIS_SENTINEL_NAME,
    REDIS_NAME: process.env.REDIS_NAME,
    REDIS_PASSWORD: process.env.REDIS_PASSWORD,
    REDIS_DB: process.env.REDIS_DB,
    GUARD_SECRET: process.env.GUARD_SECRET,
  };
};

export const envVars = sourceEnv();
