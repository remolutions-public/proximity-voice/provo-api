import { Module, Logger } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { GlobalProvidersModule } from '../global-providers.module';

import { ReporterAgent } from './reporter/reporter.agent';

const SERVICES = [Logger, ReporterAgent];

const MODULES = [HttpModule, GlobalProvidersModule];

@Module({
  imports: [...MODULES],
  providers: [...SERVICES],
  exports: [...SERVICES],
})
export class AgentModule {}
