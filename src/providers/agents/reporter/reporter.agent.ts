import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { Cron, SchedulerRegistry } from '@nestjs/schedule';
import { GlobalConfigService } from '../../services/config/config.service';
import { sleep } from '../../../libraries/sleep.lib';
import { parseBoolean } from '../../../libraries/parser.lib';
import {
  GlobalRedisService,
  EConnection,
} from '../../services/redis/redis.service';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class ReporterAgent {
  private loggerContext = ReporterAgent.name;
  isDebugging =
    this.configService.getValue('ENV').trim().toUpperCase() === 'DEV';
  isAgentRunning = false;
  lastAgentStartTime: number;
  maxAllowedAgentRunDelay = 10 * 60 * 1000;

  lastKnownActivePlayerCounts: number[] = [];
  movingAverageActivePlayerCountEntries = 20;

  constructor(
    private readonly schedulerRegistry: SchedulerRegistry,
    private readonly logger: Logger,
    private readonly configService: GlobalConfigService,
    private readonly redisService: GlobalRedisService,
    private readonly httpService: HttpService,
  ) {
    // init upon api start, don't wait for first run
    setTimeout(async () => {
      this.runAgent();
    }, 500);
  }

  @Cron('0 */5 * * * *', {
    name: 'reporter_agent_cron_job',
  })
  async runAgent(): Promise<void> {
    if (
      (this.configService.getValue('MODE') || '').trim().toLowerCase() === 'api'
    ) {
      return;
    }
    if (!this.isAgentRunning) {
      this.isAgentRunning = true;
      await sleep(Math.random() * 2450 + 50);
      this.lastAgentStartTime = new Date().valueOf();
      if (this.isDebugging) {
        this.logger.debug(
          new Date().toISOString() + ' reporter_agent_cron_job is running...',
          this.loggerContext,
        );
      }

      await this.reportBackendToProvoApi().catch((err) => {
        this.logger.error(err, undefined, this.loggerContext);
      });

      this.finishRunningAgentCycle();
    }
  }

  stopAgent(): void {
    const job = this.schedulerRegistry.getCronJob('reporter_agent_cron_job');
    job.stop();
    this.logger.debug('Stopped reporter agent cron job.', this.loggerContext);
  }

  startAgent(): void {
    const job = this.schedulerRegistry.getCronJob('reporter_agent_cron_job');
    job.start();
    this.logger.debug(
      'Reporter agent cron job was started.',
      this.loggerContext,
    );
  }

  restartAgent(): void {
    this.stopAgent();
    this.startAgent();
  }

  async reportBackendToProvoApi(): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      let errorState: any;
      const provoUrlsRaw = this.configService
        .getValue('PROVO_CONTROLLER_URLS')
        .trim()
        .split(',');

      const backendid = await this.redisService.getBackendId();
      if (backendid == undefined || !backendid.length) {
        this.logger.warn(
          'cannot report backend data, since backendid is invalid!',
          this.loggerContext,
        );
        return;
      }

      const env = this.configService.getValue('ENV');
      const isPublic = parseBoolean(
        this.configService.getValue('IS_PUBLIC_API'),
      );
      const backendName = this.configService.getValue('BACKEND_NAME');
      const backendUrl = this.configService.getValue('BACKEND_URL');

      const playerKeys = await this.redisService.scanKeys(
        EConnection.default,
        'realms:*',
      );

      this.lastKnownActivePlayerCounts.push(playerKeys.length);
      if (
        this.lastKnownActivePlayerCounts.length >
        this.movingAverageActivePlayerCountEntries
      ) {
        this.lastKnownActivePlayerCounts.shift();
      }
      let playerSum = 0;
      for (const entry of this.lastKnownActivePlayerCounts) {
        playerSum += entry;
      }
      let avgActivePlayers = 0;
      if (playerSum > 0) {
        avgActivePlayers = Math.round(
          playerSum / this.lastKnownActivePlayerCounts.length,
        );
      }

      const requestConfig = {
        withCredentials: true,
        timeout: 5000,
      };
      const promises = [];
      for (let url of provoUrlsRaw) {
        if (url.endsWith('/')) {
          url = url.slice(0, -1);
        }
        promises.push(
          firstValueFrom(
            this.httpService.post(
              url + '/api/v1/backends',
              {
                backendid,
                env,
                isPublic,
                backendName,
                backendUrl,
                avgActivePlayers,
              },
              requestConfig,
            ),
          ).catch((err) => {
            errorState = err;
          }),
        );
      }
      await Promise.all(promises);
      if (!(errorState == undefined)) {
        this.logger.error(errorState, undefined, this.loggerContext);
        return resolve(false);
      }

      resolve(true);
    });
  }

  finishRunningAgentCycle() {
    if (this.isDebugging) {
      this.logger.debug(
        new Date().toISOString() +
          ' reporter_agent_cron_job has finished. (exec time: ' +
          (new Date().valueOf() - this.lastAgentStartTime) +
          ' ms)',
        this.loggerContext,
      );
    }

    this.isAgentRunning = false;
  }
}
