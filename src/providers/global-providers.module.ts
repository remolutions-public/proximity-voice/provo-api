import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { APP_GUARD } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { ApiVersionGuard } from './guards';
import { ErrorService } from './services/error/error.service';
import { GlobalConfigService } from './services/config/config.service';
import { GlobalRedisService } from './services/redis/redis.service';
import { KubectlService } from './services/kubectl/kubectl.service';
import { GlobalMqttService } from './services/mqtt/mqtt.service';
import { Logger } from '@nestjs/common';

const SERVICES = [
  Logger,
  ErrorService,
  ConfigService,
  GlobalConfigService,
  GlobalRedisService,
  KubectlService,
  GlobalMqttService,
];

const GUARDS: any = [
  {
    provide: APP_GUARD,
    useClass: ApiVersionGuard,
  },
];

const MODULES = [ScheduleModule.forRoot()];

@Module({
  imports: [...MODULES],
  providers: [...SERVICES, ...GUARDS],
  exports: [...SERVICES],
})
export class GlobalProvidersModule {}
