// https://stackoverflow.com/questions/246801/how-can-you-encode-a-string-to-base64-in-javascript
export const encodeBase64 = (text: string) => {
  try {
    return Buffer.from(text).toString('base64');
  } catch (err) {
    console.error('btoa failed:', err);
    return text;
  }
};

export const decodeBase64 = (b64Encoded: string) => {
  try {
    return Buffer.from(b64Encoded, 'base64').toString();
  } catch (err) {
    console.error('atob failed:', err);
    return b64Encoded;
  }
};

export const typeIsBase64 = (str: string) => {
  try {
    const decoded = Buffer.from(str, 'base64').toString();
    const encoded = Buffer.from(decoded).toString('base64');
    if (encoded === str) {
      return true;
    }
    return false;
  } catch (err) {
    return false;
  }
};

export const encodeData = (dataObj: any): string => {
  if (!(dataObj == undefined)) {
    try {
      let stringifiedData = dataObj;
      if (typeof dataObj === 'object') {
        stringifiedData = JSON.stringify(dataObj);
      }
      return encodeBase64(stringifiedData);
    } catch (e) {
      console.error(e);
    }
  }
};

export const dataParse = (data: string) => {
  if (!(data == undefined) && data.length) {
    try {
      return JSON.parse(decodeBase64(data));
    } catch (e) {
      console.error(e);
    }
  }
};

export const getExpectedValue = (data: string) => {
  let expectedValue = 0;
  if (!(data == undefined) && data.length) {
    try {
      const decodedData = dataParse(data);
      if (!(decodedData == undefined) && !Number.isNaN(decodedData.value)) {
        expectedValue = parseFloat(decodedData.value || 0);
      }
    } catch (e) {
      console.error(e);
    }
  }
  return expectedValue;
};
