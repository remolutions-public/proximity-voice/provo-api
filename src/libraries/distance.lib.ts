export const distance3DTo = (
  obj1: { x: number; y: number; z: number },
  obj2: { x: number; y: number; z: number },
): number => {
  return Math.sqrt(
    Math.pow(obj2.x - obj1.x, 2) +
      Math.pow(obj2.y - obj1.y, 2) +
      Math.pow(obj2.z - obj1.z, 2),
  );
};
