export const voiceBaseSettingsSchema = `
syntax = "proto3";
package settings;

message VoiceModeEntry {
    float voiceLevel = 1;
    float maxDistancePercent = 2;
}

message VoiceModes {
    VoiceModeEntry whisper = 1;
    VoiceModeEntry talk = 2;
    VoiceModeEntry yell = 3;
}

message VoiceBaseSettingRequest {
    string type = 1;
    string clientid = 2;
    string requestid = 3;
    int64 time = 4;
}

message VoiceBaseSettingVelocityOffsets {
    float positionWhisperOffsetMultiplier = 1;
    float radiusWhisperExtensionMultiplier = 2;
    float positionTalkOffsetMultiplier = 3;
    float radiusTalkExtensionMultiplier = 4;
    float positionYellOffsetMultiplier = 5;
    float radiusYellExtensionMultiplier = 6;
    float positionOffsetDirection = 7;
}

message VoiceBaseSettings {
    float max_distance = 1; // becomes maxDistance
    VoiceModes voice_mode_keys = 2;
    VoiceBaseSettingVelocityOffsets velocity_offsets = 3;
}
`;