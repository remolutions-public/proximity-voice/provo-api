export const playerDataSchema = `
syntax = "proto3";
package players;

message PlayerVector3D {
  double x = 1;
  double y = 2;
  double z = 3;
}

message PlayerData {
  string name = 1;
  string id = 2;
  PlayerVector3D pos = 3;
  PlayerVector3D fVector = 4;
  string realm = 5;
  string serverid = 6;
  string type = 7;
  string clientid = 8;
  bool playerIsHidden = 9;
  string version = 10;
}
`;
