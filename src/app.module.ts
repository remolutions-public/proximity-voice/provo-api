import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GlobalProvidersModule } from './providers/global-providers.module';
import { Logger } from '@nestjs/common';
import { envVars } from './providers/environment';
import { RedisModule, RedisClientOptions } from '@liaoliaots/nestjs-redis';
import { HealthCheckModule } from './routes/healthcheck/healthcheck.module';
import { MqttModule } from './routes/mqtt/mqtt.module';
import { CoturnModule } from './routes/coturn/coturn.module';
import { AgentModule } from './providers/agents/agent.module';
import { parseBoolean } from './libraries/parser.lib';

const redisConfig: RedisClientOptions = {};
export const prepareRedisConfig = () => {
  if (parseBoolean(envVars.REDIS_USE_SENTINEL)) {
    redisConfig.role = 'master';
    redisConfig.name = envVars.REDIS_SENTINEL_NAME;
    redisConfig.sentinelPassword = envVars.REDIS_PASSWORD?.trim();
    redisConfig.sentinels = [
      {
        host: envVars.REDIS_SENTINEL_HOST,
        port: parseInt(envVars.REDIS_SENTINEL_PORT, 10),
      },
    ];
  } else {
    redisConfig.host = envVars.REDIS_HOST;
    redisConfig.port = parseInt(envVars.REDIS_PORT, 10);
    redisConfig.name = envVars.REDIS_NAME;
  }
  redisConfig.password = envVars.REDIS_PASSWORD?.trim();
  redisConfig.db = parseInt(envVars.REDIS_DB, 10);
  redisConfig.namespace = envVars.REDIS_NAME;
};
prepareRedisConfig();

const MODULES = [
  GlobalProvidersModule,
  RedisModule.forRoot({
    config: [redisConfig],
  }),
  ConfigModule.forRoot({
    isGlobal: true,
    envFilePath: '.env.' + envVars.ENV.toLowerCase(),
  }),
  AgentModule,
  MqttModule,
  HealthCheckModule,
  CoturnModule,
];

const SERVICES = [AppService, Logger];

@Module({
  imports: [...MODULES],
  controllers: [AppController],
  providers: [...SERVICES],
})
export class AppModule {}
