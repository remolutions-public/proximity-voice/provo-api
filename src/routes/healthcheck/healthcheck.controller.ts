import { Controller, Get } from '@nestjs/common';
import { ApiVersion } from '../../providers/guards';
import { HealthCheckService } from './healthcheck.service';

@Controller('healthcheck')
export class HealthCheckController {
  constructor(
    private readonly healthCheckService: HealthCheckService,
  ) { }

  @Get()
  @ApiVersion('v1')
  async getHealthCheck(): Promise<any> {
    return await this.healthCheckService.getHealthCheck();
  }
}
