import { HttpException, Injectable } from '@nestjs/common';
import { GlobalRedisService } from '../../providers/services/redis/redis.service';
import { GlobalConfigService } from '../../providers/services/config/config.service';
import { ErrorService } from '../../providers/services/error/error.service';
import { ReporterAgent } from '../../providers/agents/reporter/reporter.agent';

@Injectable()
export class HealthCheckService {
  constructor(
    private readonly globalRedisService: GlobalRedisService,
    private readonly globalConfigService: GlobalConfigService,
    private readonly reporterAgent: ReporterAgent,
  ) {}

  async getHealthCheck(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (!(await this.globalRedisService.getRedisHealth())) {
        return reject(new HttpException('Redis connection is invalid', 420));
      }

      if (
        (this.globalConfigService.getValue('MODE') || '')
          .trim()
          .toLowerCase() === 'agent'
      ) {
        if (
          new Date().valueOf() - this.reporterAgent.lastAgentStartTime >
          this.reporterAgent.maxAllowedAgentRunDelay
        ) {
          return reject(
            new HttpException(
              'Agent last execution time is older then ' +
                this.reporterAgent.maxAllowedAgentRunDelay +
                'ms',
              500,
            ),
          );
        }
      }

      resolve({ message: 'healthy', time: new Date().toISOString() });
    }).catch((err) => ErrorService.getInst().handle(err));
  }
}
