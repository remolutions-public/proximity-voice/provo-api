import { Module } from '@nestjs/common';
import { HealthCheckController } from './healthcheck.controller';
import { HealthCheckService } from './healthcheck.service';
import { GlobalProvidersModule } from '../../providers/global-providers.module';
import { AgentModule } from '../../providers/agents/agent.module';

const MODULES = [
  GlobalProvidersModule,
  AgentModule,
];

const CONTROLLERS = [
  HealthCheckController,
];

const SERVICES = [
  HealthCheckService,
];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class HealthCheckModule { }
