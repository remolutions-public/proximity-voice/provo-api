import { Injectable } from '@nestjs/common';
import { ErrorService } from '../../providers/services/error/error.service';
import { GlobalConfigService } from '../../providers/services/config/config.service';
import { GlobalMqttService } from '../../providers/services/mqtt/mqtt.service';

@Injectable()
export class MqttService {
  constructor(
    private readonly configService: GlobalConfigService,
    private readonly globalMqttService: GlobalMqttService,
  ) {}

  async getMqttData(req: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const clientUsername = 'provo_client';
      const mqttCreds = this.configService.getMqttPassword(clientUsername);
      return resolve({
        namespace: this.configService.getValue('NAMESPACE'),
        mqttBroker: this.configService.getValue('RABBITMQ_SERVICE_NAME'),
        controllerChannel: this.globalMqttService.controllerChannelName,
        latency: this.globalMqttService.brokerLatency,
        lastHeartbeat: this.globalMqttService.lastBrokerLatencyTime,
        healthState: this.globalMqttService.healthState,
        externalMqttHost: this.configService.getValue('RABBITMQ_EXTERNAL_HOST'),
        externalMqttWsPort: this.configService.getValue('RABBITMQ_MQTT_EXTERNAL_WS_PORT'),
        mqttUser: mqttCreds.mqttUser,
        mqttPass: mqttCreds.mqttPass,
      });
    }).catch((err) => ErrorService.getInst().handle(err));
  }
}
