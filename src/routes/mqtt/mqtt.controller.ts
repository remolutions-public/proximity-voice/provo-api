import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { ApiVersion, SecretGuard } from '../../providers/guards';
import { MqttService } from './mqtt.service';

@Controller('mqtt')
export class MqttController {
  constructor(private readonly mqttService: MqttService) {}

  @Get()
  @ApiVersion('v1')
  @UseGuards(SecretGuard)
  async getMqttData(@Req() req: any): Promise<any> {
    return await this.mqttService.getMqttData(req);
  }
}
