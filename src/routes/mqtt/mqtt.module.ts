import { Module } from '@nestjs/common';
import { MqttController } from './mqtt.controller';
import { MqttService } from './mqtt.service';
import { GlobalProvidersModule } from '../../providers/global-providers.module';

const MODULES = [GlobalProvidersModule];

const CONTROLLERS = [MqttController];

const SERVICES = [MqttService];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class MqttModule {}
