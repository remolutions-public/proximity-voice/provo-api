import { Module } from '@nestjs/common';
import { CoturnController } from './coturn.controller';
import { CoturnService } from './coturn.service';
import { GlobalProvidersModule } from '../../providers/global-providers.module';

const MODULES = [GlobalProvidersModule];

const CONTROLLERS = [CoturnController];

const SERVICES = [CoturnService];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class CoturnModule {}
