import {
  Controller,
  Get,
  Post,
  UseGuards,
  Req,
  Body,
  Query,
} from '@nestjs/common';
import { ApiVersion, SecretGuard } from '../../providers/guards';
import { CoturnService } from './coturn.service';

@Controller('coturn')
export class CoturnController {
  constructor(private readonly coturnService: CoturnService) {}

  @Get()
  @ApiVersion('v1')
  @UseGuards(SecretGuard)
  async getCoturnData(@Req() req: any): Promise<any> {
    return await this.coturnService.getCoturnData(req);
  }

  @Post()
  @ApiVersion('v1')
  @UseGuards(SecretGuard)
  async updateCoturnData(@Body() body: any, @Query() query: any): Promise<any> {
    return await this.coturnService.updateCoturnData(body, query);
  }
}
