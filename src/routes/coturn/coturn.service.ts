import { Injectable } from '@nestjs/common';
import {
  GlobalRedisService,
  EConnection,
} from '../../providers/services/redis/redis.service';
import { ErrorService } from '../../providers/services/error/error.service';

@Injectable()
export class CoturnService {
  constructor(private readonly redisService: GlobalRedisService) {}

  async getCoturnData(req: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.redisService
        .getCoturnData()
        .then((result) => {
          resolve(result);
        })
        .catch((err) => reject(err));
    }).catch((err) => ErrorService.getInst().handle(err));
  }

  async updateCoturnData(body: any, query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (query.podname == undefined) {
        return reject(new Error('no podname provided!'));
      }
      this.redisService
        .set(EConnection.coturn, `coturn:${query.podname}`, body, 30)
        .then((result) => {
          resolve(result);
        })
        .catch((err) => reject(err));
    }).catch((err) => ErrorService.getInst().handle(err));
  }
}
