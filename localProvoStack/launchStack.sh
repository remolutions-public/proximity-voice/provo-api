#!/bin/sh


BASE_PATH=$(dirname \"$0\" | tr -d '"')
cd $BASE_PATH


if [ -z "$(docker network ls --filter "name=provo" --quiet)" ]; then 
  docker network create provo
fi
docker-compose up -d


if [ -f "../.env.dev" ]; then 
  export $(xargs <../.env.dev)
fi


RABBITMQ_CONTAINER_NAME=localprovostack-provo-rabbitmq-1
WAIT_FOR_RABBITMQ=true
while $WAIT_FOR_RABBITMQ
do
  DOCKER_CONTAINER=$(docker ps -a -q --filter "status=running" --filter "name=$RABBITMQ_CONTAINER_NAME")
  if [ -z $DOCKER_CONTAINER ]; then
    continue
  fi

  WAIT_FOR_RABBITMQ=false


  echo "RABBITMQ_USERS: $RABBITMQ_USERS"


  for USER in $(echo $RABBITMQ_USERS | sed 's/,/ /g')
  do
    USERNAME=$(echo $USER | awk -F':' '{ print $1 }')
    PASSWORD=$(echo $USER | awk -F':' '{ print $2 }')

    # https://www.rabbitmq.com/access-control.html
    EXISTING_USERS=$(docker exec -i $DOCKER_CONTAINER rabbitmqctl list_users --formatter=json)
    if [ ! -z "${EXISTING_USERS##*"$USERNAME"*}" ]; then
      # echo "creating user $USERNAME"
      # CONFIG_PERMISSION=".*"
      # if [ "$USERNAME" = "provo_api" ]; then
      #   CONFIG_PERMISSION=".*"
      # fi
      # rabbitmqctl [--node <node>] [--longnames] [--quiet] set_permissions [--vhost <vhost>] <username> <conf> <write> <read>
      # docker exec -i $DOCKER_CONTAINER sh -c "rabbitmqctl add_user '$USERNAME' '$PASSWORD' && rabbitmqctl set_permissions --vhost \"/\" \"$USERNAME\" \"$CONFIG_PERMISSION\" \".*\" \".*\""
      docker exec -i $DOCKER_CONTAINER sh -c "rabbitmqctl add_user '$USERNAME' '$PASSWORD' && rabbitmqctl set_permissions --vhost \"/\" \"$USERNAME\" \".*\" \".*\" \".*\""
    fi

    # rabbitmqctl list_users --formatter=json
    # rabbitmqctl add_user 'username' 'dummypassword'
    # rabbitmqctl delete_user 'username'
    # rabbitmqctl set_permissions -p "custom-vhost" "username" ".*" ".*" ".*"
  done
done
